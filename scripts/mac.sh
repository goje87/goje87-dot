#!/bin/bash

# Show all files (including hidden files) on Mac
defaults write com.apple.finder AppleShowAllFiles YES

# Disable swipe to navigate back in Chrome
defaults write com.google.Chrome.plist AppleEnableSwipeNavigateWithScrolls -bool FALSE

# Dock settings
defaults write com.apple.dock static-only -bool true
defaults write com.apple.dock autohide -bool true
defaults write com.apple.dock mineffect -string genie
killall Dock