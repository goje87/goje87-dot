#!/bin/bash

# Install homebrew
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
brew tap homebrew/cask
brew tap homebrew/cask-versions

cat << EOF | xargs -L1 brew install --cask
spectacle
google-chrome
google-chrome-canary
vlc
webtorrent
sourcetree
visual-studio-code
evernote
iterm2
menumeters
adobe-acrobat-reader
EOF

brew install agrinman/tap/tunnelto
