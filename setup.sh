#!/bin/bash

GOJE87_DOT="$(cd "$(dirname "$0")" && pwd)"

# Make zsh the default shell
chsh -s /bin/zsh

# Configure zsh
$GOJE87_DOT/scripts/ohmyzsh.sh

# Set mac defaults
$GOJE87_DOT/scripts/mac.sh

# Install homebrew apps
$GOJE87_DOT/scripts/homebrew.sh

