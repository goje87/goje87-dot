# Goje87-dot

All the scripts required to setup a new macbook. The setup script installs [Bash-it](https://github.com/Bash-it/bash-it) and few more custom command line utilities. See `.inputrc` file for more info.

## Installation

Run the following command to install to run the setup.

```
git clone --depth=1 https://gitlab.com/goje87/goje87-dot.git ~/.goje87-dot && ~/.goje87-dot/setup.sh
```
